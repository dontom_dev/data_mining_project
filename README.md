# Title
A Retail Recommendation Engine using data mining methods.

# Description
The assignment was to create the retail recommendation engine based on random data set of product list made by the customers. Engine should have been created using Apriori Algorithm programmed in Java Script language. Implementation of the program is also using html5, css3 languages, grunt and npm tools and bootstrap framework.  Results could be helpful to increase sale of the products using shown correlations between them.
 
Final results of the retail recommendation engine should be the list of products, which are the most often bought, but also the correlations between them (associations).

# Author
DonTom - Warsaw University of Technology
Course: Data Mining
Date: 05.01.2018

### Geting Started
Open up git shell and enter the following commands
    - npm install
    - bower install
    - grunt watch
    - http-server

# Working program
http://dontom.pl/data_mining/

## Documentation
http://dontom.pl/data_mining/docs/dokumentacja/A%20Retail%20Recommendation%20Engine%20using%20data%20mining%20methods..pdf

## Technical documentation
http://dontom.pl/data_mining/docs/

## Repository
https://bitbucket.org/dontom_dev/data_mining_project 

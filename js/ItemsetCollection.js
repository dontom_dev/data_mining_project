// ItemsetCollection.js

'use strict';

/**
 * Class ItemsetCollection is responsible for managing the collections of itemset.
 * It extends the Array class methods.
 */
class ItemsetCollection extends Array {
    
    /**
     * Default contructor.
     */
    constructor() {
        super();
    }

    /**
     * Function removes all the repetitions from the array of products and returns
     * only unique results.
     */
    getUniqueItems() {
        let uniqueItems = new Itemset();

        for (var index in this) {
            let itemset = this[index];
            for (var i = 0; i < itemset.length; i += 1) {
                // includes determines whether one string may be found within another string,
                // returning true or false as appropriate
                if (!uniqueItems.includes(itemset[i])) {
                    uniqueItems.push(itemset[i]);
                }
            }
        }

        return uniqueItems;
    }

    /**
     * Function calculates support for specific order
     * @param {Itemset} itemset it list of orders stored as a object Itemset
     * @return {number} return support for specifit Itemset
     */
    findSupport(itemset) {
        let matchCount = 0;
        for (var index in this) {
            let is = this[index];
            if (is.includesItemset(itemset)) { // Itemset.js
                matchCount += 1;
            }
        }

        // Calculating support number
        let support = (matchCount / this.length) * 100.0;
        return support;
    }

    /**
     * Function clears the length
     */
    clear() {
        this.length = 0;
    }

    /**
     * Function return the string with new line joined at the end of string
     */
    toString() {
        return this.join('\n');
    }
}

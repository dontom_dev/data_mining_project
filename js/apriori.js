// Products:
// 1 - Breitling
// 2 - Zenith
// 3 - Jaeger
// 4 - IWC
// 5 - Silvana
// 6 - Edox 
// 7 - Maurice
// 8 - Vacherol

// The initial set of all products
var _products = $.trim('Breitling, Zenith, Jaeger, IWC, Silvana, Edox, Maurice, Vacherol');

var _dbGlobal = [];
var allRules; // all Association Rules

var _startingDB = [
    'IWC, Jaeger, Vacherol, Breitling',
	'Vacherol, Edox, Silvana, Jaeger',
	'IWC, Silvana, Jaeger, Edox, Vacherol, Breitling',
	'Zenith, IWC',
	'Breitling, Zenith, Edox, IWC',
	'Zenith, Silvana, Edox, Jaeger, IWC',
	'Silvana, Breitling, Jaeger',
	'Jaeger',
	'Edox',
	'Vacherol, Zenith, IWC',
	'Breitling, Maurice, Edox, IWC, Silvana',
	'Zenith, Maurice, Vacherol, IWC, Silvana',
	'Breitling, Zenith',
	'Vacherol, Maurice, Edox',
	'Edox, Breitling, Zenith',
	'Jaeger, Zenith, IWC',
	'Silvana, Breitling, Zenith, Vacherol, Edox',
	'Jaeger',
	'Edox, Maurice, Jaeger, Zenith',
	'Breitling, Jaeger, Silvana, Zenith, Vacherol, Maurice'
];

var _db = []; // final database of orders

var _numberOfOrders = 200;

// Support is an indication of how frequently the itemset appears in the dataset.
// i.e., if x appears 2 times on 8 orders, then support is 2/8=25%
var _supportThreshold = 20; 

// Confidence is an indication of how often the rule has been found to be true.
var _confidenceThreshold = 50.00;

var orderedProducts = [];
var associatedProducts = [];


/**
 * Function displays which watch was clicked (added to order)
 */
function markSelection() {
	orderedProducts.push(this.className.split(" ")[2]);
    console.log("You've ordered: " + orderedProducts);
}

function orderNow() {
	
	// console.log(orderedProducts);
	// console.log(allRules);
	var associated = 0;
	var temp = [];
	var noProductsInfo = document.getElementById("no-products");
	var associatedPromotionProducts = [];

	noProductsInfo.style.display = "block";

	var allElem = document.getElementsByClassName("inline");
	for(var i = 0; i < allElem.length; i++) {
		allElem[i].style.display = "none";
	}

	for(var i = 0, len = allRules.length; i < len; i++) {
		associatedProducts[i] = [];
		associatedProducts[i].push(allRules[i].X[0]);
		associatedProducts[i].push(allRules[i].Y[0]);

		for(var j = 0, lenProd = orderedProducts.length; j < lenProd; j++) {
			temp = []
			if(orderedProducts[j] == associatedProducts[i][0]) {
				associated = 1;
				temp = document.getElementsByClassName("inline " + associatedProducts[i][1].toUpperCase());
				for(var n = 0; n < temp.length; n++) {
					temp[n].style.display = "block";
				}
			}
		}
	}
	if(associated) {
		noProductsInfo.style.display = "none";
	}


	// Modal view for associated products
	var modal = document.getElementById('myModal');
	modal.style.display = "block";
}
	
function rerunApriori() {
	_dbGlobal = [];
	allRules = [];
	_db = [];
	orderedProducts = [];
	associatedProducts = [];

	GenerateDataBase();

	allRules = AprioriAlgorithm(_db); // initialize apriori algorithm

	GetPromotedProducts();
}

/* function */
function change_numberOfOrders() {
	if(this.value) 
		_numberOfOrders = this.value;
}
/* function */
function change_supportThreshold() {
	if(this.value) 
		_supportThreshold = this.value;
}
/* function */
function change_confidenceThreshold() {
	if(this.value) 
		_confidenceThreshold = this.value;
}

function openSettings() {
	/* event listener */
	document.getElementById("_numberOfOrders").addEventListener('change', change_numberOfOrders);
	document.getElementById("_supportThreshold").addEventListener('change', change_supportThreshold);
	document.getElementById("_confidenceThreshold").addEventListener('change', change_confidenceThreshold);

	// Modal view for associated products
	var settingsModal = document.getElementById('settingsModal');
	settingsModal.style.display = "block";

	var runAprioriModal = document.getElementById("run_apriori_modal");
	runAprioriModal.onclick = rerunApriori;
}


/**
 * Main function of a Retail Recommendation Engine responsible for 
 * initializing all functions: 
 * 		GenerateDataBase(), AprioriAlgorithm(_db) and GetPromotedProducts();
 */
$(function() {
	GenerateDataBase();
	// _supportThreshold = 10.00;

	allRules = AprioriAlgorithm(_db); // initialize apriori algorithm
	// AprioriAlgorithm(_dbGlobal);

	GetPromotedProducts();

	var products = document.getElementsByClassName("product-left");
	for (i = 0, len = products.length; i < len; i++){
		products[i].onclick = markSelection;
	}

	var orderNowBtn = document.getElementById("order_now");
	orderNowBtn.onclick = orderNow;


	// Modal view for settings
	var settingsModal = document.getElementById('settingsModal');
	// Get the <span> element that closes the settingsModal
	var settingsSpan = document.getElementsByClassName("closeSettings")[0];


	// Modal view for associated products
	var modal = document.getElementById('myModal');
	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];


	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		modal.style.display = "none";
	}
	// When the user clicks on <span> (x), close the modal
	settingsSpan.onclick = function() {
		settingsModal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}

		if (event.target == settingsModal) {
			settingsModal.style.display = "none";
		}
	}

	var runApriori = document.getElementById("run_apriori");
	runApriori.onclick = rerunApriori;

	var settings = document.getElementById("settings");
	settings.onclick = openSettings;
});


/**
 * Function responsible for generating random orders of length 
 * _numberOfOrders given assigned at the beginning of the file.
 * By default _numberOfOrders = 200.
 * Each order is consisted by random number of products without 
 * repetitions and of the maximal length of all available products.
 */
function GenerateDataBase() {
    var products = _products.split(','); // array of products

    for(var i in products) {
        products[i] = $.trim(products[i])
	}

	// _db = _startingDB;
	// console.log(_db);

    // Generate random database of products
	var numberOfProducts;
    for(var i = 0; i < _numberOfOrders;) { // for each row
		numberOfProducts = GetRandomInt(1, products.length-1);
		// console.log("Number of products: " + numberOfProducts);
		var productSet = []; // list of all orders with products

		for(var j = 0; j < numberOfProducts; j++) {
			var random = GetRandomInt(1, products.length);
			var item = products[random - 1];
			// console.log("Product: " + j + " - " + item);
			
			if(productSet.indexOf(item) < 0) {
				productSet.push(item);
			}
		}

		if(productSet.length > 0) {
			this._db.push($.trim(productSet.join(', ')));
			i++;
		}
	}

	_dbGlobal = [];
	_dbGlobal = _startingDB;
}

/**
 * Function generate random variable in interval from min to max.
 * @param {number} min minimum value of the interval 
 * @param {number} max maximal value of the interval 
 * @returns {number} return generated random number
 */
function GetRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Function prints given text in console and into ResultTextBox input.
 * @param {string} text text which will be displayed
 */
function AddResultLine(text) {
	console.log(text);
    $('#ResultTextBox').val($('#ResultTextBox').val() + text + '\n');
}

/**
 * Function prints given text into ResultTextBox input.
 * @param {string} text text which will be displayed
 */
function AddResultLineNoConsole(text) {
    $('#ResultTextBox').val($('#ResultTextBox').val() + text + '\n');
}


/**
 * Function clears ResultTextBox input.
 */
function ClearResult() {
    $('#ResultTextBox').val('');
}

/**
 * Function compares a and b numbers and returns them in ascending order.
 * @param {number} a given value to compare with b
 * @param {number} b given value to compare with a
 */
function sortFunction(a, b) {
	if (a[1] === b[1]) {
		return 0;
	}
	else {
		return (a[1] > b[1]) ? -1 : 1;
	}
}

/**
 * Function finds all products in ResultTextBox input, 
 * splits them to get two dimensional array of name and value.
 * Then sorts them in a descending order and display 3 products without repetitions
 * as a sponsored products. Function display also sponsored products in console.
 */
function FindSponsoredProducts() {
	// Hide all sponsored products
	$(".product-name").each(function(index) {
		$(this).hide();
	});

	var list = $('#ResultTextBox').val();
	list = list.split('\n');
	// console.log(list);

	var row = [];
	for(var i = 0; i < list.length; i++) {
		if(list[i].length > 0) {
			row[i] = list[i].split('|');
			row[i] = list[i].split(':');
			row[i][1] = $.trim(row[i][1]);
			row[i][1] = parseFloat(row[i][1]) / 100.0;
		}
	}
	row.sort(sortFunction);
	// console.log(row);

	var promo = 3; // number of promoted products
	var promotedProducts = [];
	var temp;
	var j = 0;
	for(var i = 0; i < promo;) {
		temp = row[j][0].split("|");

		// Check if promotedProducts includes only one element
		if(temp[0].indexOf(",") == -1) {
			promotedProducts.push($.trim(temp[0]));
			// Show all sponsored products
			// console.log(promotedProducts[i]);
			$("#" + promotedProducts[i]).show();
			i++; j++;
		}else {
			j++;
		}		
	}

	console.log('\n'+"Sponsored products:");
	console.log(promotedProducts);
}


/**
 * Function creates ItemsetCollection, creates the set of orders in appropriate format
 * and run apriori algorithm on given dataset only to get the promoted product list.
 */
function GetPromotedProducts() {
	var db = new ItemsetCollection(); // ItemsetCollection.js
	for (var i in _db) {
		var items = _db[i].split(','); // split each order into array and pass it into Itemset
		db.push(Itemset.from(items)); // Itemset.js
	}

	// Step1: Find large itemsets for given support threshold
	var supportThreshold = _supportThreshold;
	var L = AprioriMining.doApriori(db, supportThreshold);

	ClearResult();
	AddResultLineNoConsole(L.join('\n'));
	AddResultLine('');

	FindSponsoredProducts();
}


/**
 * Function creates ItemsetCollection, creates the new set of orders in appropriate format
 * and run apriori algorithm on given dataset to get all large item sets and correlations
 * between products.
 */
function AprioriAlgorithm(tempDB) {
	console.log(tempDB);

	var db = new ItemsetCollection(); // ItemsetCollection.js
	for (var i in tempDB) {
		var items = tempDB[i].split(', '); // split each order into array and pass it into Itemset
		db.push(Itemset.from(items)); // Itemset.js
	}

	// Step1: Find large itemsets for given support threshold
	var supportThreshold = _supportThreshold;
	var L = AprioriMining.doApriori(db, supportThreshold);

	ClearResult();
	AddResultLine(L.length + ' Large Itemsets (by Apriori):');
	AddResultLine(L.join('\n'));
	AddResultLine('');

	// Step2: Build rules based on large itemsets and confidence threshold
	var confidenceThreshold = _confidenceThreshold;
	allRules = AprioriMining.mine(db, L, confidenceThreshold);
	AddResultLine(allRules.length + " Association Rules");
	AddResultLine(allRules.join('\n'));

	return allRules;
}


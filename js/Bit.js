// Bit.js

'use strict';

/**
 * Bit class transforms orders into bit representation and creates permutations of
 * all items in specific order.
 */
class Bit {

    /**
     * Functions creates all permutations of products inside itemset of length n.
     * @returns {ItemsetCollection} returns list of all permuted itemso flength n
     */
    static findSubsets(itemset, n) {
        let subsets = new ItemsetCollection();

        let subsetCount = Math.pow(2, itemset.length);
        for (var i = 0; i < subsetCount; i++) {
            if (n == 0 || Bit.getOnCount(i, itemset.length) == n) {
                let binary = Bit.decimalToBinary(i, itemset.length);

                let subset = new Itemset();
                for (var charIndex = 0; charIndex < binary.length; charIndex++) {
                    if (binary[binary.length - charIndex - 1] == '1') {
                        subset.push(itemset[charIndex]);
                    }
                }
                subsets.push(subset);
            }
        }

        return subsets;
    }

    /**
     * Function transforms decimal number to binary representation.
     * @returns {boolean} returns true if bitwise AND operation is bigger than true
     */
    static getBit(value, position) {
        // Bitwise AND - Returns a one in each bit position for which
        // the corresponding bits of both operands are ones.
        let bit = value & Math.pow(2, position);
        return (bit > 0 ? 1 : 0);
    }

    /**
     * Transforms decimal number into binary representation.
     * @returns {bollean} return binary string
     */
    static decimalToBinary(value, length) {
        let binary = '';
        for (var position = 0; position < length; position++) {
            binary = Bit.getBit(value, position) + binary;
        }
        return binary;
    }

    /**
     * Function calculating number of 1's inside the string.
     * @param {number} value is value which will to be transformed into binary
     * @param {number} length is length of binary string
     * @returns {number} returns number of ones 
     */
    static getOnCount(value, length) {
        let binary = Bit.decimalToBinary(value, length);

        let onCount = 0;
        for (var i = 0; i < binary.length; i += 1) {
            if (binary[i] == '1') {
                onCount += 1;
            }
        }

        return onCount;
    }
}

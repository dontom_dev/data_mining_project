// Itemset.js

'use strict';

/**
 * Class Itemset is responsible to store all products. It extends default Array class.
 * The class defines also methods, which are required to process Apriori algorithm operations.
 */
class Itemset extends Array {
    
    /**
     * Class constructor, by default defines support as 0.
     */
    constructor() {
        super();
        this.Support = 0.0;
    }
    
    /**
     * Function searches for itemset inside reference object
     * @param {Itemset} itemset it is object which is searched
     * @returns {bool} return true is itemset include specific order, otherwise false
     */
    includesItemset(itemset) {
        for (var i = 0; i < itemset.length; i += 1) {
            var item = itemset[i];
            if (!this.includes(item)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Function removes specific subset of elements from main itemset
     * @param {Itemset} itemset 
     * @returns {Itemset} return itemset with specific subset removed
     */
    removeItemset(itemset) {
        var removed = new Itemset();
        for (var i = 0; i < this.length; i += 1) {
            var item = this[i]; // default itemset from which we remove items
            if (!itemset.includes(item)) {
                removed.push(item);
            }
        }
        return removed;
    }

    /**
     * Function return strings for 
     * @returns {string} return string i.e. "{Zenith, Breitling}"
     */
    toStringNoSupport() {
        return '{' + this.join(', ') + '}';
    }

    /**
     * Function return strings for 
     * @returns {string} return string i.e. "Zenith,Breitling"
     */
    toStringNoSupportBlank() {
        return this.join(',');
    }

    /**
     * Function return strings for Large Itemsets (by Apriori)
     * @returns {string} return string i.e. "Zenith | support: 38%"
     */
    toString() {
        return '' + this.join(', ') + ' | support: ' + this.Support + '%';
    }
}
